import { Component, ViewContainerRef } from '@angular/core';
import { SliderComponent } from './slider/slider.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent{
  title = 'assessment'
  constructor(private viewContainerRef: ViewContainerRef) {}

  addNewComponent() {
    this.viewContainerRef.createComponent(SliderComponent);
  }
}
