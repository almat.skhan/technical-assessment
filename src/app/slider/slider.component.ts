import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Slide, SlidesService } from './slides.service';
import { Observable, Subscription, interval } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit, OnDestroy{
  numImages: number = 3
  startIndex: number = 0
  endIndex: number = this.numImages
  time: number = 1
  width = `max-width: calc(95vw / ${this.numImages})`
  sub!: Subscription;
  slidesSub!: Subscription
  source!: any
  
  ngOnInit(): void {
    this.source = interval(this.time * 1000)
    this.sub = this.source
      .subscribe(() => { this.changeSlides() });
    this.slides$.subscribe((slides: Slide[]) => {
      this.orderedSlides = slides
    })
  }

  constructor(
    private slidesService: SlidesService,
    private httpClient: HttpClient,
    private changeDetection: ChangeDetectorRef
  ) {

  }

  slides$ = this.slidesService.getSlides()
  orderedSlides: Slide[] = []
  slicedSlides: Slide[] = []


  clicked(slide: Slide): Observable<any> {
    // Open the image in new tab. 
    window.open(slide.url, "_blank");
    return this.httpClient.get(slide.url);
  }

  onNumChange() {
    this.width = `max-width: calc(95vw / ${this.numImages})`
  }

  onTimeChange() {
    this.sub.unsubscribe()
    this.source = interval(this.time * 1000)
    this.sub = this.source.subscribe(() => {
      this.changeSlides()
    })
  }

  changeSlides() {
    const num = this.numImages
    
    if (this.endIndex > this.orderedSlides.length) {
      this.endIndex -= this.orderedSlides.length
      this.slicedSlides = (this.orderedSlides.slice(this.startIndex)).concat(this.orderedSlides.slice(0, this.endIndex))
    }
    else if (this.startIndex >= this.orderedSlides.length) {
      this.startIndex -= this.orderedSlides.length
      this.endIndex -= this.orderedSlides.length
      this.slicedSlides = this.orderedSlides.slice(this.startIndex, this.endIndex)
    }
    else {
      this.slicedSlides = this.orderedSlides.slice(this.startIndex, this.endIndex)
      this.startIndex += num
      this.endIndex += num
    }
    this.changeDetection.detectChanges()
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe()
    this.slidesSub.unsubscribe()
  }
}
