import { Injectable, OnInit } from "@angular/core";
import { Observable, of } from "rxjs";

export interface Slide {
    id: number;
    image: string;
    url: string;
    priority: number;
  }
  

@Injectable({
  providedIn: 'root'
})
export class SlidesService{

  private slides: Slide[] = [
    { id: 1, image: 'url1', url: "https://images.unsplash.com/photo-1704642155498-70b60672f1f3?q=80&w=1974&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D", priority: 1 },
    { id: 2, image: 'url2', url: "https://images.unsplash.com/photo-1704122545548-385c09c16e22?q=80&w=2002&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D", priority: 3 },
    { id: 3, image: 'url3', url: "https://images.unsplash.com/photo-1682695795255-b236b1f1267d?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D", priority: 2 },
    { id: 4, image: 'url4', url: "https://images.unsplash.com/photo-1682685794304-99d3d07c57d2?q=80&w=1972&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D", priority: 2 },
    { id: 5, image: 'url5', url: "https://images.unsplash.com/photo-1682688759457-52bcb4dc1578?q=80&w=1974&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D", priority: 3 },
    { id: 6, image: 'url6', url: "https://images.unsplash.com/photo-1682685797741-f0213d24418c?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D", priority: 2 },
    { id: 7, image: 'url7', url: "https://images.unsplash.com/photo-1704098712161-67949aaf0eee?q=80&w=1974&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D", priority: 4 },
    { id: 8, image: 'url8', url: "https://images.unsplash.com/photo-1682687219356-e820ca126c92?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D", priority: 2 },
    { id: 9, image: 'url9', url: "https://images.unsplash.com/photo-1704741253008-7b0a5ed52c12?q=80&w=2128&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D", priority: 5 },
  ];

  
  sortSlides(slides: Slide[]): Slide[] {
    let orderedSlides: Slide[] = []
    slides.sort((a, b) => b.priority - a.priority);
    for (let slide of this.slides) {
      for (let i = 0; i < slide.priority; i++) {
        orderedSlides.push(slide);
      }
    }

    return orderedSlides
  }

  getSumOfSlides(): number {
    let sum = 0;
    for (let slide of this.slides) {
      sum += slide.priority
    }

    return sum
  }

  getSlides(): Observable<Slide[]> {
    return of(this.sortSlides(this.slides));
  }
}